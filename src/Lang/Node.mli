
type ('meta, 'data) node =
  { meta : 'meta
  ; data : 'data
  }
