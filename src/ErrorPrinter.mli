
val error    : Box.t list -> Box.t
val error_p  : Utils.Position.t -> Box.t list -> Box.t
val error_pu : Flow.metadata -> Utils.UID.t -> Box.t list -> Box.t
